const fastify = require('fastify')({ logger: true })

const port = process.env.OPENSHIFT_NODEJS_PORT || 8080
const ipAddress = process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0'

fastify.route({
  method: 'GET',
  url: '/',
  schema: {
    querystring: {
      name: { type: 'string' }
    },
    response: {
      200: {
        type: 'object',
        properties: {
          hello: { type: 'string' }
        }
      }
    }
  },
  preHandler: async (req, reply) => {
    console.log({ req });
  },
  handler: async (req, reply) => {
    return { hello: 'world' }
  }
})

const start = async () => {
  try {
    await fastify.listen(port, ipAddress)
  } catch (err) {
    fastify.log.error(err)
    process.exit(1)
  }
}

start()