FROM node:14.17

LABEL "maintainer": "adam.berkowitz@uconn.edu" \
  "name": "Adam Berkowitz" \
  "project": "openshift node test"

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm set progress=false \
 && npm install

COPY . .

EXPOSE 8080

ENTRYPOINT ["npm", "run", "start"]
